<?php

namespace App\Domain\Actions;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Sanctum\NewAccessToken;

class CreateTokenAction
{
    public function execute(array $fields): NewAccessToken
    {
        /** @var User $user */
        $user = User::where('login', $fields['login'])->first();


        if (!$user || !Hash::check($fields['password'], $user->password)) {
            throw ValidationException::withMessages(["Неверный логин или пароль"]);
        }

        return $user->createToken($fields['login']);

    }
}
