<?php

namespace App\Domain\Actions;

use App\Models\User;

class UpdateUserAction
{
    public function execute(int $id, array $fields): User
    {
        /** @var User $user */
        $user = User::findOrFail($id);

        $user->update($fields);

        return $user;
    }
}
