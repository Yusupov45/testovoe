<?php

namespace App\Domain\Enums;

enum RoleEnum: string
{
    case ADMIN = "admin";
    case USER = "user";

}
