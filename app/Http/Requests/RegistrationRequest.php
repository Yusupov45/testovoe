<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistrationRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'login' => ['required', 'string', 'unique:users'],
            'password' => ['required', 'string']
        ];
    }
}
