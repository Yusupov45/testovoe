<?php

namespace App\Http\Requests;

use App\Domain\Enums\RoleEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'role' => [Rule::in(RoleEnum::cases())],
            'is_blocked' => ['bool']
        ];
    }
}
