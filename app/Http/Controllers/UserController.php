<?php

namespace App\Http\Controllers;

use App\Domain\Actions\DeleteUserAction;
use App\Domain\Actions\UpdateUserAction;
use App\Http\Queries\UserQuery;
use App\Http\Requests\RegistrationRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class UserController
{
    public function registration(RegistrationRequest $request): UserResource
    {
        return new UserResource(User::create($request->validated())); // пароль хэшируется (настроено в модели)
    }

    public function get(int $id): UserResource
    {
        return new UserResource(User::findOrFail($id));
    }

    public function update(int $id, UpdateUserRequest $request, UpdateUserAction $action): UserResource
    {
        return new UserResource($action->execute($id, $request->validated()));
    }

    public function search(UserQuery $query): AnonymousResourceCollection
    {
        return UserResource::collection($query->get());
    }

    public function delete(int $id, DeleteUserAction $action): JsonResponse
    {
        $action->execute($id);

        return response()->json(['data' => null]);
    }
}
