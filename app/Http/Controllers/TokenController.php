<?php

namespace App\Http\Controllers;

use App\Domain\Actions\CreateTokenAction;
use App\Http\Requests\CreateTokenRequest;
use App\Http\Resources\TokenResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class TokenController extends Controller
{
    public function createToken(CreateTokenRequest $request, CreateTokenAction $action): TokenResource
    {
        return new TokenResource($action->execute($request->validated()));
    }

    public function deleteToken(Request $request): JsonResponse
    {
        $request->user()?->currentAccessToken()->delete();

        return response()->json(['data' => null]);
    }
}
