<?php

namespace App\Http\Middleware;

use App\Domain\Enums\RoleEnum;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminMiddleware
{
    public function handle(Request $request, Closure $next): Response
    {
        $user = $request->user();

        if (!$user || $user->role !== RoleEnum::ADMIN) {
            throw new AuthenticationException("Недостаточно прав");
        }

        return $next($request);
    }
}
