<?php

namespace App\Http\Queries;

use App\Models\User;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class UserQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(User::query());

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::partial('login'),
            AllowedFilter::exact('role'),
            AllowedFilter::exact('is_blocked')
        ]);

        $this->allowedSorts([
            'id',
            'login',
            'updated_at',
            'created_at',
            'role',
            'is_blocked'
        ]);
    }
}
