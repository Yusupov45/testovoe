<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Domain\Enums\RoleEnum;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory()->create([
            'login' => 'admin',
            'password' => Hash::make("admin"),
            'role' => RoleEnum::ADMIN,
            'is_blocked' => false
        ]);

        User::factory()->create([
            'login' => 'user',
            'password' => Hash::make("user"),
            'role' => RoleEnum::USER,
            'is_blocked' => false
        ]);
    }
}
